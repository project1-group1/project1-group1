import express from 'express';

// Added by Leung on 5:04pm 15-Jul-20
export const isLoggedIn = function (req: express.Request, res: express.Response, next: express.NextFunction) {

    if (req.session && req.session.user) {
        next();
    } else {
        res.status(401).redirect("/");
    }
}
  // Added by Leung on 5:04pm 15-Jul-20