import express from 'express';
import multer from 'multer';
import { client } from './app';

export const missionRoutes = express.Router();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/public/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
const upload = multer({ storage });


/**
 * Create new mission
 */
missionRoutes.post('/mission', upload.single('image'), async (req, res) => {     // "isLoggedInAPI" Added by Leung on 5:04pm 15-Jul-20

    // Assign the file of an image to "image". If none, assign noimage1.jpg
    const image = req.file ? req.file.filename : "noimage1.jpg";

    const mission = req.body;

    let response1 = await client.query(`select * from categories where name = $1`, [mission.category]);
    const category_id = response1.rows[0].id;


    // Search the db to see whether the new mission delivery_place is previously entered
    const response2 = await client.query(`select * from mission_locations where location = $1`, [mission.deliveryPlace]);
    const foundLocation = response2.rows[0];

    let locationId;
    if (foundLocation) {
        // delivery_place is previously entered

        locationId = foundLocation.id;
    }
    else {
        // Otherwise, insert a new location and get back a new id 

        const response3 = await client.query(`insert into mission_locations (location) values ($1) returning id`, [mission.deliveryPlace]);
        locationId = response3.rows[0].id;
    }
    if (req.session) {

    /*let response4 = */ await client.query(`insert into missions (contributor_id, image, title, content, delivery_time, delivery_place_id,
        expiration, coin_demand, max_patrons, serving, category_id, question, created_at, updated_at) values 
        ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, NOW(), NOW()) returning id`,
        [req.session.user.id, image, mission.title, mission.content, mission.deliveryTime,
            locationId, mission.expiration, mission.coinDemand, mission.maxPatrons, mission.serving, category_id, mission.question]);

        // const missionId = response4.rows[0].id;

        // await client.query(`insert into patrons (mission_id, question) values ($1, $2)`, [missionId, mission.question]);

        res.json({ message: "new mission added" });
    }
    else {

        res.json({ message: "error - new mission cannot be added" });
    }
});

/**
 * Get all the name of mission_locations
 */
missionRoutes.get('/locations', async (req, res) => {

    let response = await client.query(`select location from mission_locations`);
    const locations = response.rows;

    res.json(locations);
});

////////////////////// Added by Leung on 7:00pm 18-Jul-20 /////////////////////////////
/**
 * 
 */
missionRoutes.get('/current-user', async function (req, res) {
    if (req.session && req.session.user) {

        const response = await client.query(`select * from users where id = $1`, [req.session.user.id]);
        const user = response.rows[0];

        if (req.session.user.picture) {
            user.picture = req.session.user.picture;
        }

        req.session.user = user;

        user.password = null;

        res.json(user);
    } else {
        res.status(401).json({ message: "Current user not found." });
    }
})
/////////////////////// Added by Leung on 7:00pm 18-Jul-20 ////////////////////////////

/**
 * Get a single mission data with missions.id
 */
missionRoutes.get('/mission/:id', async (req, res) => {

    const { id } = req.params;

    const response =
        await client.query(`select users.firstname, users.lastname, title, content, image, serving,
            coin_demand, delivery_time, location, question, max_patrons, expiration, missions.updated_at from missions 
            join mission_locations on mission_locations.id = missions.delivery_place_id 
            join categories on categories.id = missions.category_id 
            join users on users.id = missions.contributor_id 
            left outer join patrons on patrons.mission_id = missions.id where missions.id = $1`, [id]);

    const mission = response.rows[0];

    res.json(mission);
});

missionRoutes.get('/is-join/:missionId/:patronId', async (req, res) => {

    const { missionId, patronId } = req.params;

    const response = await client.query(`select * from patrons where mission_id = $1 and patron_id = $2`,
        [missionId, patronId])

    const result = response.rows;

    res.json(result);
})

/**
 * Get all missions data
 */
missionRoutes.get('/missions', async (req, res) => {

    if (req.session) {
        const response =
            await client.query(`select missions.id as mission_id, content, image, title, serving, contributor_id,
            coin_demand, delivery_time, location, max_patrons, expiration from missions 
            join mission_locations on mission_locations.id = missions.delivery_place_id 
            join categories on categories.id = missions.category_id`);

        const missions = response.rows;

        res.json(missions);
    }
    else {
        res.status(400).json({ message: 'fail' });
    }
});

/**
 * Get all missions data that the current user has joined
 */
missionRoutes.get('/join-histories', async (req, res) => {

    if (req.session && req.session.user) {
        const response =
            await client.query(`select missions.id as mission_id, patrons.id as event_id, missions.serving,
                patrons.is_completed, missions.image, missions.title, missions.content, missions.contributor_id,
                missions.contributor_id, missions.updated_at, missions.delivery_place_id,
                missions.delivery_time, missions.coin_demand, missions.max_patrons,
                missions.expiration, missions.question, patrons.answer, missions.created_at from patrons
                join missions on missions.id = patrons.mission_id
                where patrons.patron_id = $1 order by missions.created_at`, [req.session.user.id]);

        const result = response.rows;
        res.json(result);
    }
    else {
        res.status(400).json({ message: 'fail' });
    }
})

/**
 * Get join history with partons.id
 */
missionRoutes.get('/join-history/:eventId', async (req, res) => {

    const { eventId } = req.params;

    if (req.session && req.session.user) {
        const response =
            await client.query(`select missions.id as mission_id, patrons.id as event_id,
                patrons.is_completed, missions.image, missions.title, missions.content,
                missions.contributor_id, missions.updated_at, missions.delivery_place_id,
                missions.delivery_time, missions.coin_demand, missions.max_patrons,
                missions.expiration, missions.question, missions.serving, patrons.answer from patrons
                join missions on missions.id = patrons.mission_id
                where patrons.id = $1`, [eventId]);

        const result = response.rows[0];

        res.json(result);

    }
    else {

        res.json({ message: 'users is not logged in' });
    }
})

/**
 * 
 */
missionRoutes.get('/income-histories', async (req, res) => {

    if (req.session && req.session.user) {

        const response =
            await client.query(`select missions.id as mission_id, missions.title,
                missions.created_at, missions.coin_demand, patrons.patron_id as patron_id,
                patrons.is_completed from missions
                join patrons on patrons.mission_id = missions.id
                where contributor_id = $1
                order by missions.created_at asc`, [req.session.user.id]);

        const result = response.rows;
        res.json(result);
    }
    else {
        res.json({ message: 'user is not logged in' });
    }
})

/**
 * Get all missions data that the current user has created
 */
missionRoutes.get('/contribute-histories', async (req, res) => {

    if (req.session && req.session.user) {

        const response =
            await client.query(`select missions.id, missions.image, title, content, delivery_time, serving,
                mission_locations.location, coin_demand, max_patrons, expiration, question from missions
                join mission_locations on mission_locations.id = missions.delivery_place_id
                where contributor_id =  $1`, [req.session.user.id]);

        const result = response.rows;
        res.json(result);

    }
    else {

        res.json({ message: 'user is not logged in' });
    }
})

missionRoutes.get('/conversations/:mission_id/:patron_id', async (req, res) => {

    const missionId = parseInt(req.params.mission_id);
    const patronId = parseInt(req.params.patron_id);

    console.log(missionId);
    console.log(patronId);

    if (req.session && req.session.user) {

        const response =
            await client.query(`select missions.title,
            missions.created_at as mission_created_at,
            users.firstname,
            users.lastname,
            messages.message,
            messages.created_at,
            messages.is_patron_msg
             from messages
             join missions on messages.mission_id  = missions.id
             join users on messages.patron_id = users.id
             where mission_id = $1 and patron_id = $2
             order by messages.created_at desc`, [missionId, patronId]);

        const result = response.rows;
        res.json(result)
    }
    else {

        res.json({ message: 'user is not logged in' });
    }
})

/**
 * Create a new joined patron event with the current user
 */
missionRoutes.post('/patron', async (req, res) => {

    const patronInfo = req.body;

    if (req.session) {

        const userId = req.session.user.id;

        await client.query(`insert into patrons (answer, patron_id, mission_id, is_completed) 
            values ($1, $2, $3, $4)`, [patronInfo.answer, userId, patronInfo.mission_id, false]);

        res.json({ success: 'true' });
    }
    else {
        res.status(400).json({ success: 'fail' });
    }

})

missionRoutes.post('/send-patron', async (req, res) => {

    const messageInfo = req.body;

    if (req.session) {

        await client.query(`insert into messages (mission_id, patron_id, message, is_patron_msg, created_at) 
            values ($1, $2, $3, $4, NOW())`, [messageInfo.missionId, messageInfo.patronId, messageInfo.message, false]);

        res.json({ success: 'true' });
    }
    else {
        res.status(400).json({ success: 'fail' });
    }
})

missionRoutes.post('/send-contributor', async (req, res) => {

    const messageInfo = req.body;

    if (req.session) {

        await client.query(`insert into messages (mission_id, patron_id, message, is_patron_msg, created_at) 
            values ($1, $2, $3, $4, NOW())`, [messageInfo.missionId, messageInfo.patronId, messageInfo.message, true]);

        res.json({ success: 'true' });
    }
    else {
        res.status(400).json({ success: 'fail' });
    }
})

/**
 * Get a list of joined patrons with with missions.id
 */
missionRoutes.get('/patrons/:id', async (req, res) => {

    const { id } = req.params;

    const response =
        await client.query(`select users.firstname, users.lastname, users.mobile, patrons.answer, is_completed from patrons  
            join users on users.id = patrons.patron_id
            where patrons.mission_id = $1`, [id]);

    const result = response.rows;
    res.json(result);
});

/**
 * Get the name of a location with mission_locations.id
 */
missionRoutes.get('/location/:id', async (req, res) => {

    const { id } = req.params;

    const response1 = await client.query(`select location from mission_locations where id = $1`, [id]);

    const location = await response1.rows[0];

    res.json(location);
})

/**
 * Get the is_completed status with patrons.id
 */
missionRoutes.get('/is-completed/:id', async (req, res) => {

    const { id } = req.params;

    const response = await client.query(`select is_completed from patrons where id = $1`, [id]);

    const result = response.rows[0];

    res.json(result);
});

/**
 * Get the answer of a user with patrons.id
 */
missionRoutes.put('/answer/:eventId', async (req, res) => {

    const { eventId } = req.params;

    const message = req.body;

    await client.query(`update patrons set answer = $1 where id = $2`, [message.answer, eventId]);

    res.json({ success: "true" });
})

/**
 * Get the question of a user with missions.id return empty string if it is not existed
 */
missionRoutes.get('/question/:id', async (req, res) => {

    const { id } = req.params;

    const response1 = await client.query(`select question from missions where id = $1`, [id]);

    const question = await response1.rows[0];

    if (question) {
        res.json(question);
    }
    else {

        res.json({ question: "" });
    }
})

/**
 * Get the total number of patrons that a single mission has with missions.id
 */
missionRoutes.get('/num-of-patrons/:id', async (req, res) => {

    const { id } = req.params;

    const response = await client.query(`select count(patrons.patron_id) from missions
        left outer join patrons on patrons.mission_id = missions.id where missions.id = $1
        group by missions.id`, [id])

    const numOfPatrons = response.rows[0];

    res.json(numOfPatrons);
});

/**
 * Get a list of patron names of a single mission with missions.id
 */
missionRoutes.get('/list-of-patrons/:id', async (req, res) => {

    const { id } = req.params;

    const response =
        await client.query(`select patrons.id as patron_id, patrons.patron_id as joiner_id, users.firstname, users.lastname from patrons 
            join users on users.id = patrons.patron_id where mission_id = $1;`, [id])

    const listOfPatrons = response.rows;

    res.json(listOfPatrons);
});

/**
 * Get the name of the contributor with users.id
 */
missionRoutes.get('/contributor-name/:id', async (req, res) => {

    const { id } = req.params;

    const response = await client.query(`select firstname, lastname from users where id = $1;`, [id])

    const contributor = response.rows[0];

    res.json(contributor);
})

/**
 * 
 */
missionRoutes.get('/list-of-missions/:id', async (req, res) => {

    const { id } = req.params;

    const response = await client.query(`select id as mission_id, missions.title, missions.created_at from missions where contributor_id = $1;`, [id])

    const list = response.rows;

    res.json(list);
})


/**
 * Pay the contributor for a specified patron with patrons.id
 */
missionRoutes.get('/pay-for-contributor/:eventId', async (req, res) => {

    const { eventId } = req.params;

    const response = await client.query(`select * from patrons where id = $1`, [eventId]);
    const event = await response.rows[0];

    const response1 = await client.query(`select coin_demand, contributor_id from missions where missions.id = $1`, [event.mission_id]);
    const mission = await response1.rows[0];

    const response2 = await client.query(`select firstname, num_coin from users where id = $1`, [event.patron_id]);
    const patron = await response2.rows[0];

    const response3 = await client.query(`select firstname, num_coin from users where id = $1`, [mission.contributor_id]);
    const contributor = await response3.rows[0];

    await client.query(`update users set num_coin = $1 where id = $2`, [patron.num_coin - mission.coin_demand, event.patron_id]);
    await client.query(`update users set num_coin = $1 where id = $2`, [contributor.num_coin + mission.coin_demand, mission.contributor_id]);
    await client.query(`update patrons set is_completed = $1 where id = $2`, [true, eventId]);

    res.json({ sucess: true });
})

///////////////////////// Added by Leung on 8:32pm 19-Jul-20 ///////////////////////////////

/**
 * Get the total amount of coins that a user (patron) that WILL pay for the joined incompleted mission
 */
missionRoutes.get('/coins-outstanding/:id', async (req, res) => {

    const { id } = req.params;

    const result = await client.query(`select sum(missions.coin_demand) from missions
        join patrons on patrons.mission_id = missions.id where patrons.patron_id = $1
        and (patrons.is_completed is NULL OR patrons.is_completed = 'false')
        group by patrons.patron_id`, [id])

    const sumOfCoinDemand = result.rows[0];

    if (sumOfCoinDemand) {
        res.json(sumOfCoinDemand);
    } else {
        res.json({ sum: 0 });
    }

});
///////////////////////// Added by Leung on 8:32pm 19-Jul-20 ///////////////////////////////


/**
 * Update mission title with missions.id
 */
missionRoutes.put('/title/:id', async (req, res) => {

    const { id } = req.params;

    const response = await
        client.query(`update missions set title = $1, updated_at = NOW() where id = $2`, [req.body.editedTitle, id]);

    const result = response.rows[0];

    res.json(result);
});

/**
 * Update mission question with missions.id
 */
missionRoutes.put('/question/:id', async (req, res) => {

    const { id } = req.params;

    const response =
        await client.query(`update missions set question = $1, updated_at = NOW() where id = $2`, [req.body.editedQuestion, id]);

    const result = response.rows[0];

    res.json(result);
});

/**
 * Update mission content with missions.id
 */
missionRoutes.put('/content/:id', async (req, res) => {

    const { id } = req.params;

    await client.query(`update missions set content = $1, updated_at = NOW() where id = $2`,
        [req.body.editedContent, id]);

    res.json({ success: true });
});


/**
 * Update mission deliver_time with missions.id
 */
missionRoutes.put('/delivery-time/:id', async (req, res) => {

    const { id } = req.params;

    await client.query(`update missions set delivery_time = $1 , updated_at = NOW() 
        where id = $2`, [req.body.editedDTime, id]);

    res.json({ success: true });
});

/**
 * Update mission expiration with missions.id
 */
missionRoutes.put('/expiration/:id', async (req, res) => {

    const { id } = req.params;

    await client.query(`update missions set expiration = $1 , updated_at = NOW() 
        where id = $2`, [req.body.editedETime, id]);

    res.json({ success: true });
});


/**
 * Update mission deliver_place with missions.id
 */
missionRoutes.put('/delivery-place/:id', async (req, res) => {

    const { id } = req.params;

    await client.query(`update missions set delivery_place = $1 , updated_at = NOW() 
        where id = $2`, [req.body.editedDPlace, id]);

    res.json({ success: true });
});

/**
 * Update mission serving & coin_demand with missions.id
 */
missionRoutes.put('/serving-n-demand/:id', async (req, res) => {

    const { id } = req.params;

    await client.query(`update missions set serving = $1 , coin_demand = $2, updated_at = NOW() 
      where id = $3`, [req.body.editedServing, req.body.editedCoinDemand, id]);

    res.json({ success: true });
});

/**
 * Update mission max_patron with missions.id
 */
missionRoutes.put('/max-patrons/:id', async (req, res) => {

    const { id } = req.params;

    await client.query(`update missions set max_patrons = $1 , updated_at = NOW() 
      where id = $2`, [req.body.editedMaxPatrons, id]);

    res.json({ success: true });
});


/**
 * Delete a patron (un-join a mission) with patrons.id
 */
missionRoutes.delete('/mission/:id', async (req, res) => {

    const id = parseInt(req.params.id);

    if (isNaN(id)) {
        res.status(400).json({ "message": "id not a number!!!" });
        return;
    }

    await client.query(`delete from missions where id = $1`, [id]);

    res.status(200).json({ sucess: true });
});

/**
 * Delete a mission with mission.id
 */
missionRoutes.delete('/patron/:eventId', async (req, res) => {

    const id = parseInt(req.params.eventId);

    if (isNaN(id)) {
        res.status(400).json({ "message": "id not a number!!!" });
        return;
    }

    await client.query(`delete from patrons where id = $1`, [id]);

    res.status(200).json({ sucess: true });
});


missionRoutes.get('/user-name/:id', async (req, res) => {

    const { id } = req.params;

    const response = await client.query(`select firstname, lastname from users where id = $1`, [id]);
    const name = response.rows[0];

    res.json(name);
})

missionRoutes.get('/list-of-joined/:id', async (req, res) => {

    const { id } = req.params;

    const response = await client.query(`select mission_id from patrons where patron_id = $1`, [id]);
    const list = response.rows;

    res.json(list);
})




