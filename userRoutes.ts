import express from 'express';
import { hashPassword, checkPassword } from './hash';
import { client } from './app';
import fetch from 'node-fetch';

export const userRoutes = express.Router();

userRoutes.get('/login/google', async (req, res) => {

    const accessToken = req.session?.grant.response.access_token;

    const googleResponse = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
        headers: {
            "Authorization": `Bearer ${accessToken}`
        }
    })
    const googleUser = await googleResponse.json();

    const response1 = await client.query(`select * from users where username = $1`, [googleUser.email]);
    const users = response1.rows;
    const found = users[0];

    if (found) {

        if (req.session) {
            req.session.user = found;
            req.session.user.picture = googleUser.picture;
        }
    }
    else {

        const password = await hashPassword(Math.random().toString(36).substring(2));

        const response2 =
            await client.query(`insert into users (username, password, firstname, lastname, mobile,
                num_coin, created_at, updated_at) values ($1, $2, $3, $4, $5, $6, NOW(), NOW()) returning id`,
                [googleUser.email, password, googleUser.given_name, googleUser.family_name, '', 10]);

        const userId = await response2.rows[0].id;

        const response3 = await client.query(`select * from users where id = $1`, [userId]);
        const user = await response3.rows[0];

        if (req.session) {
            req.session.user = user;
            req.session.user.picture = googleUser.picture;
        }
    }
    res.redirect('/home.html');

})

userRoutes.post('/login', async (req, res) => {

    const user = req.body;

    const response = await client.query(`select * from users where username = $1`, [user.email]);
    const found = response.rows[0];

    if (found && await checkPassword(user.password, found.password)) {

        if (req.session) {

            req.session.user = found;
            res.status(200).json({ success: true });
        }
    }
    else {
        res.status(401).json({ success: false });
    }

})

userRoutes.post('/signup', async (req, res) => {

    const user = req.body;

    // Search the users table to see whether the user has registered before
    const response = await client.query(`select * from users where username = $1`, [user.email]);
    const found = response.rows[0];

    if (found) {
        res.status(401).json({ message: 'This email address has been registered before.' });
    }
    else {

        user.password = await hashPassword(user.password);

        // Insert new user into the database
        await client.query(`insert into users (username, password, firstname, lastname, mobile, num_coin, created_at, updated_at) values ($1, $2, $3, $4, $5, $6, NOW(), NOW())`, [user.email, user.password, user.firstname, user.lastname, user.mobile, 10]);

        res.json({ message: 'New user has been successful added' });
    }
})

userRoutes.post('/signup', async (req, res) => {

    const user = req.body;

    // Search the users table to see whether the user has registered before
    const response = await client.query(`select * from users where username = $1`, [user.email]);
    const found = response.rows[0];

    if (found) {
        res.status(401).json({ message: 'This email address has been registered before.' });
    }
    else {

        user.password = await hashPassword(user.password);

        // Insert new user into the database
        await client.query(`insert into users (username, password, firstname, lastname, mobile, num_coin, created_at, updated_at) values ($1, $2, $3, $4, $5, $6, NOW(), NOW())`, [user.email, user.password, user.firstname, user.lastname, user.mobile, 10]);

        res.json({ message: 'New user has been successful added' });
    }
})

userRoutes.get('/logout', (req, res) => {

    if (req.session && req.session.user) {
        req.session.user = null;
        res.status(200).json({ sucess: true });
    } else {
        res.status(400).json({ sucess: false });
    }
})

userRoutes.post('/contact-us', async (req, res) => {

    const contact = req.body;

    // Insert contact into the database
    await client.query(`insert into contact (firstname, lastname, email, mobile, message, created_at, updated_at) values ($1, $2, $3, $4, $5, NOW(), NOW())`, [contact.firstname, contact.lastname, contact.email, contact.mobile, contact.message]);

    // res.redirect('/index.html');   // Commented by Leung on 1:00pm 25-Jul-20
    res.json({ message: 'Contact has been successfully added' });

})