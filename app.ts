import express from 'express';
import expressSession from 'express-session';
// import { Request, Response, NextFunction } from 'express';
import path from 'path';
import bodyParser from 'body-parser';
// import jsonfile from 'jsonfile';
import pg from 'pg';
import dotenv from 'dotenv';
import socketIO from 'socket.io';
import http from 'http';
import { isLoggedIn } from './guard';
import { userRoutes } from './userRoutes';
import { missionRoutes } from './missionRoutes';
// import { NextFunction } from 'connect';
import grant from 'grant-express';



dotenv.config();

const PORT = 8080;
const app = express();
const server = new http.Server(app);
const io = socketIO(server);

export const client = new pg.Client({
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  host: "localhost",
  port: 5432
})

client.connect();


const sessionMiddleware = expressSession({
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false }
});

app.use(sessionMiddleware);



app.use(grant({
  "defaults": {
    "protocol": "http",
    "host": process.env.HOST || "localhost:8080",
    "transport": "session",
    "state": true,
  },
  "google": {
    "key": process.env.GOOGLE_CLIENT_ID || "",
    "secret": process.env.GOOGLE_CLIENT_SECRET || "",
    "scope": ["profile", "email"],
    "callback": "/login/google"
  },
}));

io.use((socket, next) => {
  sessionMiddleware(socket.request, socket.request.res, next);
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/', userRoutes);
app.use('/', missionRoutes);









/**
 * Client post a new mission to server
 */
// app.post('/mission', upload.single('image'), async (req, res) => {     // "isLoggedInAPI" Added by Leung on 5:04pm 15-Jul-20

//   // Assign the file of an image to "image". If none, assign empty string.
//   const image = req.file ? req.file.filename : "";

//   const mission = req.body;

//   let response1 = await client.query(`select * from categories where name = $1`, [mission.category]);
//   const category_id = response1.rows[0].id;


//   // Search the db to see whether the new mission delivery_place is previously entered
//   const response2 = await client.query(`select * from mission_locations where location = $1`, [mission.deliveryPlace]);
//   const foundLocation = response2.rows[0];

//   let locationId;
//   if (foundLocation) {
//     // delivery_place is previously entered

//     locationId = foundLocation.id;
//   }
//   else {
//     // Otherwise, insert a new location and get back a new id

//     const response3 = await client.query(`insert into mission_locations (location) values ($1) returning id`, [mission.deliveryPlace]);
//     locationId = response3.rows[0].id;
//   }
//   if (req.session) {

//     /*let response4 = */ await client.query(`insert into missions (contributor_id, image, title, content, delivery_time, delivery_place_id,
//         expiration, coin_demand, max_patrons, serving, category_id, question, created_at, updated_at) values 
//         ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, NOW(), NOW()) returning id`,
//     [req.session.user.id, image, mission.title, mission.content, mission.deliveryTime,
//       locationId, mission.expiration, mission.coinDemand, mission.maxPatrons, mission.serving, category_id, mission.question]);

//     // const missionId = response4.rows[0].id;

//     // await client.query(`insert into patrons (mission_id, question) values ($1, $2)`, [missionId, mission.question]);

//     res.json({ message: "new mission added" });
//   }
//   else {

//     res.json({ message: "error - new mission cannot be added" });
//   }
// });











































app.use(express.static('public'));

app.use(isLoggedIn, express.static('protected'));

app.use((req, res) => {

  res.sendFile(path.join(__dirname, 'public/404.html'));
})

server.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
})