/**
 * Handle contact form
 */
document.querySelector(".contact-btn").addEventListener("click", async function (event) {
    event.preventDefault();

    const contactForm = document.querySelector("#contact-form");

    if(contactForm.checkValidity() === false){
        event.preventDefault();
        event.stopPropagation();
        contactForm.classList.add('was-validated');
        return;
    }

        const response = await fetch('/contact-us', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                firstname: contactForm.elements.firstname.value,
                lastname: contactForm.elements.lastname.value,
                email: contactForm.elements.email.value,
                mobile: contactForm.elements.mobile.value,
                message: contactForm.elements.message.value,
            })
        });
    
        const result = await response.json();
    
        alert("我們已收到你的資料。");   ///////// Added by Leung on 1:00pm 25-Jul-20
        contactForm.reset();
    
        window.location = '/';         ///////// Added by Leung on 1:00pm 25-Jul-20
    }
);