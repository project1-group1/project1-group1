-- drop the database, and create a new one
DROP DATABASE mission;
CREATE DATABASE mission;
CREATE TABLE users(
    id SERIAL primary key,
    username VARCHAR(255),
    password VARCHAR(255),
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    mobile VARCHAR(255),
    num_coin INTEGER,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);
CREATE TABLE mission_locations(
    id SERIAL primary key,
    location VARCHAR(255)
);
CREATE TABLE categories(
    id SERIAL primary key,
    name VARCHAR(255)
);
CREATE TABLE missions(
    id SERIAL primary key,
    contributor_id INTEGER,
    image VARCHAR(255),
    title VARCHAR(255),
    content text,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    delivery_time TIMESTAMP,
    delivery_place_id INTEGER,
    expiration TIMESTAMP,
    coin_demand INTEGER,
    max_patrons INTEGER,
    serving VARCHAR(255),
    category_id INTEGER,
    question text,
    FOREIGN KEY (contributor_id) REFERENCES users(id),
    FOREIGN KEY (delivery_place_id) REFERENCES mission_locations(id),
    FOREIGN KEY (category_id) REFERENCES categories(id)
);
CREATE TABLE patrons(
    id SERIAL primary key,
    mission_id INTEGER,
    patron_id INTEGER,
    answer text,
    is_completed boolean,
    FOREIGN KEY (mission_id) REFERENCES missions(id),
    FOREIGN KEY (patron_id) REFERENCES users(id)
);
CREATE TABLE contact(
    id SERIAL primary key,
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    email VARCHAR(255),
    mobile VARCHAR(255),
    message text,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);
CREATE TABLE messages(
    id SERIAL primary key,
    mission_id INTEGER,
    patron_id INTEGER,
    message text,
    is_patron_msg boolean,
    created_at TIMESTAMP,
    FOREIGN KEY (mission_id) REFERENCES missions(id),
    FOREIGN KEY (patron_id) REFERENCES users(id)
);
INSERT INTO categories (name)
VALUES ('送餐');
INSERT INTO categories (name)
VALUES ('代購');
INSERT INTO categories (name)
VALUES ('代運');
INSERT INTO categories (name)
VALUES ('交通');
INSERT INTO categories (name)
VALUES ('專業技能');
INSERT INTO categories (name)
VALUES ('社區服務');
--stop here
select missions.id as mission_id,
    image,
    title,
    serving,
    coin_demand,
    delivery_time,
    location,
    max_patrons,
    expiration
from missions
    join mission_locations on mission_locations.id = missions.delivery_place_id
    join categories on categories.id = missions.category_id;
select users.firstname,
    users.lastname,
    title,
    content,
    image,
    serving,
    coin_demand,
    delivery_time,
    location,
    max_patrons,
    expiration
from missions
    join mission_locations on mission_locations.id = missions.delivery_place_id
    join categories on categories.id = missions.category_id
    join users on users.id = missions.contributor_id
    left outer join patrons on patrons.mission_id = missions.id
where missions.id = 14;
select users.firstname,
    users.lastname,
    title,
    content,
    image,
    serving,
    coin_demand,
    delivery_time,
    location,
    max_patrons,
    expiration,
    patrons.patron_id as patronId,
    patrons.question
from missions
    join mission_locations on mission_locations.id = missions.delivery_place_id
    join categories on categories.id = missions.category_id
    join users on users.id = missions.contributor_id
    left outer join patrons on patrons.mission_id = missions.id
where missions.id = 14;
select users.firstname as contributor_firsttname,
    users.lastname as contributor_firsttname,
    title,
    content,
    image,
    serving,
    coin_demand,
    delivery_time,
    location,
    max_patrons,
    expiration,
    patrons.patron_id as patronId,
    question
from missions
    join mission_locations on mission_locations.id = missions.delivery_place_id
    join categories on categories.id = missions.category_id
    join users on users.id = missions.contributor_id
    left outer join patrons on patrons.mission_id = missions.id
select count(patrons.patron_id)
from missions
    left outer join patrons on patrons.mission_id = missions.id
where missions.id = 2
group by missions.id;
select users.firstname,
    users.lastname
from patrons
    join users on users.id = patrons.patron_id
where mission_id = 2;
select missions.title,
    missions.content,
    missions.contributor_id,
    missions.updated_at,
    missions.delivery_place_id,
    missions.delivery_time,
    missions.coin_demand,
    missions.max_patrons,
    missions.expiration,
    patrons.answer
from patrons
    join missions on missions.id = patrons.mission_id
where patrons.id = 2;
select missions.id title,
    content,
    delivery_time,
    mission_locations.location,
    coin_demand,
    max_patrons,
    expiration,
    question
from missions
    join mission_locations on mission_locations.id = missions.delivery_place_id
where contributor_id = 1;
select users.firstname,
    users.lastname,
    patrons.answer
from patrons
    join users on users.id = patrons.patron_id
where patrons.mission_id = 1;
select missions.id as mission_id,
    missions.title,
    missions.created_at,
    missions.coin_demand,
    patrons.patron_id as patron_id,
    patrons.is_completed
from missions
    join patrons on patrons.mission_id = missions.id
where contributor_id = 1
order by missions.created_at asc
select missions.id,
    missions.title,
    missions.created_at,
    messages.is_patron_msg,
    messages.message,
    messages.patron_id,
    messages.created_at
from messages
    left outer join missions on missions.id = messages.mission_id
where missions.id = 1
order by missions.title asc,
    missions.created_at desc,
    messages.message desc
select missions.title,
    missions.created_at as mission_created_at,
    users.firstname,
    users.lastname,
    messages.message,
    messages.created_at,
    messages.is_patron_msg
from messages
    join missions on messages.mission_id = missions.id
    join users on messages.patron_id = users.id
where mission_id = 1
    and patron_id = 2
order by messages.created_at desc,
    select missions.title,
    missions.created_at as missions_created_at,
    missions.contributor_id,
    messages.message,
    messages.is_patron_msg,
    messages.created_at
from messages
    join missions on missions.id = messages.mission_id
where patron_id = 1
    and mission_id = 2
order by messages.created_at desc;