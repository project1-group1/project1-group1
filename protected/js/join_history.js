const searchParams = new URLSearchParams(location.search);
const eventId = searchParams.get('event_id');

/**
 * Handle log out
 */
document.querySelector(".logout").addEventListener("click", async function (event) {

    const response = await fetch("/logout");

    const result = await response.json();

    if (response.status === 200) {
        window.location = '/';
    }
    else {
        console.log(result);
    }

});

async function loadJoinHistory() {

    loadProfileButtonAndBalacne();

    const response1 = await fetch(`/join-history/${eventId}`);
    const joinHistory = await response1.json();

    const response2 = await fetch(`/contributor-name/${joinHistory.contributor_id}`);
    const contributor = await response2.json();

    const response3 = await fetch(`/num-of-patrons/${joinHistory.mission_id}`);
    const numOfPatrons = await response3.json();

    const response4 = await fetch(`/location/${joinHistory.delivery_place_id}`);
    const location = await response4.json();

    const response5 = await fetch(`/patrons/${joinHistory.mission_id}`);
    const listOfPatrons = await response5.json();

    let payBtnHTMl = "";
    let cxlBtnHTML = "";
    let ansEditHTML = "";

    if (joinHistory.is_completed) {
        payBtnHTMl = `<div class="pay-btn disable">巳付</div>`
        cxlBtnHTML = `<div class="cxl-btn d-none">取消</div>`;
        ansEditHTML = `<div class="edit-wrapper">
            <div class="answer">${joinHistory.answer}</div>
            <div class="edit-btn d-none"><i class="fas fa-pencil-alt"></i></div>
            </div>`
    }
    else {
        payBtnHTMl = `<div class="pay-btn">付款</div>`;
        cxlBtnHTML = `<div class="cxl-btn">取消</div>`;
        ansEditHTML = `<div class="edit-wrapper">
            <div class="answer text-editable" contenteditable="true">${joinHistory.answer}</div>
            <div class="edit-btn"><i class="fas fa-pencil-alt"></i></div>
            </div>`
    }


    let nameList = "";
    for (const patron of listOfPatrons)
        nameList += `${patron.firstname} ${patron.lastname}&nbsp&nbsp&nbsp&nbsp`;

    // Reder the page below
    const missionContainer = document.querySelector(".mission-container");
    missionContainer.innerHTML = "";

    missionContainer.innerHTML =
        `<div class="row">
                <div class="col">
                    <div class="image-frame">
                        <img src="./uploads/${joinHistory.image}" alt="">
                    </div>
                </div>
                <div class="col">
                    <h4 class="mission-title">${joinHistory.title}</h4>
                    <p class="mission-content">${joinHistory.content}</p>
                    <i class="mission-lastupdated">這項任務由 ${contributor.firstname} ${contributor.lastname}提供 任務最後更新 ${convertTime(joinHistory.updated_at)}</i>
                    <p class="mission-dtime">交收時間: ${convertTime(joinHistory.delivery_time)}</p>
                    <p>交收地點: ${location.location}</p>
                    <div class="ask-for">${joinHistory.serving}:&nbsp&nbsp
                        <div class="m-coin-sign">
                            <div class="m-sign">M</div>
                            <div class="double-line">| |</div>
                        </div>
                        &nbsp${joinHistory.coin_demand}&nbsp作報酬
                    </div>
                    <p>名額: ${joinHistory.max_patrons - numOfPatrons.count} 個 (總數: ${joinHistory.max_patrons} 個)</p>
                    <p>截止時間: ${convertTime(joinHistory.expiration)}</p>
                    <i class="ans-msg">之前你給對方的訊息:</i>
                    ${ansEditHTML}
                    <div class="btn-group">
                        ${payBtnHTMl}
                        ${cxlBtnHTML}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="namelist-label">這項任務有以下的支持者</p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <i class="patron-namelist">${nameList}</i>
                </div>
            </div>`


    document.querySelector(".pay-btn").addEventListener('click', async () => {

        const response2 = await fetch(`/pay-for-contributor/${eventId}`)
        const result2 = await response2.json();

        loadJoinHistory();
    })


    // cancel patrons is done
    document.querySelector(".cxl-btn").addEventListener('click', async () => {

        let response = await fetch(`/patron/${eventId}`, {
            method: "DELETE"
        });

        const result = await response.json();

        if (response.status === 200) {
            window.location = '/home.html';
        }
    });

    document.querySelector(".edit-btn").addEventListener('click', async () => {

        const expiration = new Date(convertTime(joinHistory.expiration));
        const now = new Date();

        if (expiration < now) {
            alert(`已過了截止時間，你不可以對訊息進行修改。`)
        }
        else {
            const newAnswer = document.querySelector(".answer").innerHTML;

            const response = await fetch(`/answer/${eventId}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ answer: newAnswer })
            });

            const result = await response.json();
        }

        loadJoinHistory();
    })
}

async function loadProfileButtonAndBalacne() {

    const response1 = await fetch('/current-user');
    const user = await response1.json();

    const response2 = await fetch(`/coins-outstanding/${user.id}`);
    const coinNeeded = await response2.json();

    let profileIcon = document.querySelector(".profile-icon")

    if (user.picture) {
        profileIcon.style.backgroundImage = `url("${user.picture}")`;
        profileIcon.style.backgroundSize = "40px 40px";
    }
    else {

        const userIntial = user.firstname[0] + user.lastname[0];
        profileIcon.innerHTML = userIntial.toUpperCase();
    }

    document.querySelector(".actual-amount").innerHTML = user.num_coin;
    document.querySelector(".avaliable-amount").innerHTML = user.num_coin - coinNeeded.sum;
}

loadJoinHistory();

