const searchParams = new URLSearchParams(location.search);
const missionId = searchParams.get('id');

/**
 * Handle log out
 */
document.querySelector(".logout").addEventListener("click", async function (event) {

    const response = await fetch("/logout");

    const result = await response.json();

    if (response.status === 200) {
        window.location = '/';
    }
    else {
        console.log(result);
    }

});

async function loadContributeHistory() {

    loadProfileButtonAndBalacne();

    const response1 = await fetch(`/mission/${missionId}`);
    const contributeHistory = await response1.json();

    const response3 = await fetch(`/num-of-patrons/${missionId}`);
    const numOfPatrons = await response3.json();

    const response5 = await fetch(`/patrons/${missionId}`);
    const listOfPatrons = await response5.json();

    let answers = "";
    for (const patron of listOfPatrons) {

        if (patron.is_completed) {
            answers += `<div class="joiner-message">${patron.firstname} ${patron.lastname} (mobile: ${patron.mobile}): ${patron.answer}<div class="paid-icon">&#x2714;PAID</div></div>`;
        }
        else {
            answers += `<div class="joiner-message">${patron.firstname} ${patron.lastname} (mobile: ${patron.mobile}): ${patron.answer}</div>`
        }
    }


    // Reder the page below
    const missionContainer = document.querySelector(".mission-container");
    missionContainer.innerHTML = "";

    missionContainer.innerHTML =
        `<div class="row">
            <div class="col">
                <div class="image-frame">
                    <img src="./uploads/${contributeHistory.image}" alt="">
                </div>
            </div>
            <div class="col">
                <div class="item i-1">
                    <h4 class="mission-title text-editable" contenteditable="true">${contributeHistory.title}</h4>
                    <div class="edit-btn e-1"><i class="fas fa-pencil-alt"></i></div>
                </div>
                <div class="item i-2">
                    <p class="mission-content text-editable" contenteditable="true">${contributeHistory.content}</p>
                    <div class="edit-btn e-2"><i class="fas fa-pencil-alt"></i></div>
                </div>
                <i class="mission-lastupdated">這項任務由 本人 提供 任務最後更新 ${convertTime(contributeHistory.updated_at)}</i>
                <div class="time-label">交收時間:</div>
                <div class="item i-3">
                    <input type="date" class="d-date" value="${convertTime(contributeHistory.delivery_time).substr(0, 10)}">
                    <input type="time" class="d-time" value="${convertTime(contributeHistory.delivery_time).substr(11)}">
                    <div class="edit-btn e-3"><i class="fas fa-pencil-alt"></i></div>
                </div>
                <div class="item i-4">
                    <div class="text-label">交收地點:</div>
                    <div class="d-place text-editable" contenteditable="true">${contributeHistory.location}</div>
                    <div class="edit-btn e-4"><i class="fas fa-pencil-alt"></i></div>
                </div>
                <div class="item i-5">
                    <div class="serving text-editable" contenteditable="true">${contributeHistory.serving}</div>
                    <div>:&nbsp;&nbsp;&nbsp;</div>
                    <div class="m-coin-sign">
                        <div class="m-sign">M</div>
                        <div class="double-line">| |</div>
                    </div>
                    <input type="number" min="0" class="coin-demand" value="${contributeHistory.coin_demand}"/>
                    <div>
                        作報酬
                    </div>
                    <div class="edit-btn e-5"><i class="fas fa-pencil-alt"></i></div>
                </div>
                <div class="item i-6">
                    <div>
                        名額: ${numOfPatrons.count} 個已參加
                    </div>
                    <div>
                        &nbsp;&nbsp;(總數:&nbsp;&nbsp;
                    </div>
                    <input type="number" min="0" class="max-patrons" value="${contributeHistory.max_patrons}"/>
                    <div>
                        &nbsp;&nbsp;個)
                    </div>
                    <div class="edit-btn e-6"><i class="fas fa-pencil-alt"></i></div>
                </div>
                <div class="time-label">
                    截止時間:
                </div>
                <div class="item i-7">
                    <input type="date" class="e-date" value="${convertTime(contributeHistory.expiration).substr(0, 10)}">
                    <input type="time" class="e-time" value="${convertTime(contributeHistory.expiration).substr(11)}"}>
                    <div class="edit-btn e-7"><i class="fas fa-pencil-alt"></i></div>
                </div>
                <p>你給參加者的訊息:</p>
                <div class="item i-8">
                    <div class="text-editable question" contenteditable="true">${contributeHistory.question}</div>
                    <div class="edit-btn e-8"><i class="fas fa-pencil-alt"></i></div>
                </div>
                <div class="btn-group">
                    <div class="cxl-btn">取消</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p class="namelist-label">參加者的留言:</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                ${answers}
            </div>
        </div>`

    /**
     * Handle update title
     */
    document.querySelector('.e-1').addEventListener('click', async () => {

        if (listOfPatrons.length > 0)
            alert(`你正更改一個已有參加者的任務，如有需要請聯絡參加者有關的更改。`)

        const editedTitle = document.querySelector('.mission-title').innerHTML;

        const response = await fetch(`/title/${missionId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ editedTitle: editedTitle })
        });

        loadContributeHistory();
    });

    /**
     * Handle update content
     */
    document.querySelector('.e-2').addEventListener('click', async () => {

        if (listOfPatrons.length > 0)
            alert(`你正更改一個已有參加者的任務，如有需要請聯絡參加者有關的更改。`)

        const editedContent = document.querySelector('.mission-content').innerHTML;

        const response = await fetch(`/content/${missionId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ editedContent: editedContent })
        });

        loadContributeHistory();
    })

    /**
     * Handle update delievery_time
     */
    document.querySelector('.e-3').addEventListener('click', async () => {

        if (listOfPatrons.length > 0)
            alert(`你正更改一個已有參加者的任務，如有需要請聯絡參加者有關的更改。`)

        const dDate = document.querySelector(".d-date").value;
        const dTime = document.querySelector(".d-time").value;

        const eDate = document.querySelector(".e-date").value;
        const eTime = document.querySelector(".e-time").value;

        const deliveryT = new Date(`${dDate}T${dTime}:00`)
        const expiration = new Date(`${eDate}T${eTime}:00`)

        if (expiration > deliveryT) {
            alert(`交收時間不可以比截止時間早`);
            return;
        }
        else {
            const editedDTime = dDate + " " + dTime;

            const response = await fetch(`/delivery-time/${missionId}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ editedDTime: editedDTime })
            });
        }

        loadContributeHistory();
    })

    /**
     * Handle update delievery_place
     */
    document.querySelector('.e-4').addEventListener('click', async () => {

        if (listOfPatrons.length > 0)
            alert(`你正更改一個已有參加者的任務，如有需要請聯絡參加者有關的更改。`)

        // delievery_place content
        const editedDPlace = document.querySelector(".d-place").innerHTML;

        const response = await fetch(`/delivery-place/${missionId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ editedDPlace: editedDPlace })
        });

        loadContributeHistory();
    })

    /**
     * Handle update serving & coin_demand
     */
    document.querySelector('.e-5').addEventListener('click', async () => {

        if (listOfPatrons.length > 0)
            alert(`你正更改一個已有參加者的任務，如有需要請聯絡參加者有關的更改。`)

        // delievery_place content
        const editedServing = document.querySelector(".serving").innerHTML;
        const editedCoinDemand = document.querySelector(".coin-demand").value;

        const response = await fetch(`/serving-n-demand/${missionId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                editedServing: editedServing,
                editedCoinDemand: editedCoinDemand
            })
        });

        loadContributeHistory();
    })

    /**
     * Handle update max_patrons
     */
    document.querySelector('.e-6').addEventListener('click', async () => {

        if (listOfPatrons.length > 0)
            alert(`你正更改一個已有參加者的任務，如有需要請聯絡參加者有關的更改。`)

        const editedMaxPatrons = document.querySelector(".max-patrons").value;

        const response = await fetch(`/max-patrons/${missionId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ editedMaxPatrons: editedMaxPatrons })
        });

        loadContributeHistory();
    })

    /**
    * Handle update expiration
    */
    document.querySelector('.e-7').addEventListener('click', async () => {

        if (listOfPatrons.length > 0)
            alert(`你正更改一個已有參加者的任務，如有需要請聯絡參加者有關的更改。`)

        const dDate = document.querySelector(".d-date").value;
        const dTime = document.querySelector(".d-time").value;

        const eDate = document.querySelector(".e-date").value;
        const eTime = document.querySelector(".e-time").value;

        const deliveryT = new Date(`${dDate}T${dTime}:00`)
        const expiration = new Date(`${eDate}T${eTime}:00`)

        if (expiration > deliveryT) {
            alert(`截止時間不可以比交收時間不可以比遲`);
            return;
        }
        else {
            const editedETime = eDate + " " + eTime;

            const response = await fetch(`/expiration/${missionId}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ editedETime: editedETime })
            });

            loadContributeHistory();
        }
    })

    /**
    * Handle update question
    */
    document.querySelector('.e-8').addEventListener('click', async () => {

        if (listOfPatrons.length > 0)
            alert(`你正更改一個已有參加者的任務，如有需要請聯絡參加者有關的更改。`)

        // question
        let editedQuestion = document.querySelector(".question").innerHTML;

        const response = await fetch(`/question/${missionId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ editedQuestion: editedQuestion })
        });

        loadContributeHistory();
    })

    document.querySelector(".cxl-btn").addEventListener("click", async () => {

        if (listOfPatrons.length > 0) {

            alert(`你不可以刪除一個巳有參加者豹任務，如有需要請聯絡我們。`)
            return
        }

        let response = await fetch(`/mission/${missionId}`, {
            method: "DELETE"
        })

        let result = await response.json();

        if (response.status === 200) {
            window.location = '/home.html';
        }

    })

}

async function loadProfileButtonAndBalacne() {

    const response1 = await fetch('/current-user');
    const user = await response1.json();

    const response2 = await fetch(`/coins-outstanding/${user.id}`);
    const coinNeeded = await response2.json();

    let profileIcon = document.querySelector(".profile-icon")

    if (user.picture) {
        profileIcon.style.backgroundImage = `url("${user.picture}")`;
        profileIcon.style.backgroundSize = "40px 40px";
    }
    else {

        const userIntial = user.firstname[0] + user.lastname[0];
        profileIcon.innerHTML = userIntial.toUpperCase();
    }

    document.querySelector(".actual-amount").innerHTML = user.num_coin;
    document.querySelector(".avaliable-amount").innerHTML = user.num_coin - coinNeeded.sum;
}

loadContributeHistory();

